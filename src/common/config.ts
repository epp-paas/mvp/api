import 'reflect-metadata';

import { get } from 'lodash';

export interface DB {
  DRIVER: string;
  HOST: string;
  PORT?: number;
  NAME: string;
  MAX_POOL?: number;
  CONNECTION_TIMEOUT?: number;
}

export interface REDIS {
  HOST: string;
  PORT?: number;
  PASSWORD?: string;
}

export interface HTTP {
  SSL_PATH?: string;
  PORT?: number;
}

export interface ApiConfig {
  http: HTTP;
  db: DB;
  redis: REDIS;
}

export interface Config<C = unknown> {
  cfg: C;

  get<P extends keyof C, R extends C[P]>(part: P, key: keyof C[P]): R;
}

export abstract class ConfigProvider<C> implements Config<C> {
  abstract get cfg(): C;

  protected abstract init(): void;

  get<P extends keyof C, R extends C[P]>(part: P, key: keyof C[P]): R {
    return get(this.cfg, [part, key]);
  }
}
