import { Security } from '../i18n/system.keys';

export interface PasswordRegItem {
  regex: string;
  required: string;
  msg: Security;
}

export const passwordRegs: PasswordRegItem[] = [
  {
    regex: '(?=.*[A-Z])',
    required: 'A-Z',
    msg: 'security:password_required_capital',
  },
  {
    regex: '(?=.*[a-z])',
    required: 'a-z',
    msg: 'security:password_required_lowercase',
  },
  {
    regex: '(?=.*[0-9])',
    required: '0-9',
    msg: 'security:password_required_number',
  },
  {
    regex: '(?=.*[!@#$%^&*()_+=-\\{}[\\]<>?/,.\'":;])',
    required: '!@#$%^&*()_+=-\\{}[\\]<>?/,.\'":;',
    msg: 'security:password_required_symbols',
  },
  {
    regex: '(?=.{8,})',
    required: 'minimum_length',
    msg: 'security:password_minimal_length',
  },
];
