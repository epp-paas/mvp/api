import { ColumnOptions } from 'typeorm';

export const nullable: ColumnOptions = Object.assign({
  nullable: true,
  default: null,
});

export const createdAt: ColumnOptions = Object.freeze({
  type: 'timestamptz',
  name: 'created_at',
});

export const label: ColumnOptions = Object.assign({
  type: 'varchar',
  length: 50,
});
