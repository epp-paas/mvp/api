import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn, ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Group as IGroup } from '../types';
import { createdAt, label, nullable } from '../../../common/entity';
import { schema } from '../common';
import { Privilege } from './privilege';

@Index('group_name_idx', ['name'])
@Entity({ ...schema, name: 'groups' })
export class Group implements Readonly<Partial<IGroup>> {
  @PrimaryGeneratedColumn('uuid')
  readonly id?: string;

  @Column(label)
  readonly name?: string;

  @Column('text')
  readonly description?: string | null;

  @Column({ type: 'uuid', name: 'owner_id' })
  readonly ownerId?: string;

  @Column({ ...nullable, type: 'uuid', name: 'parent_group_id' })
  readonly parentGroupId?: string | null;

  @ManyToOne(() => Group, undefined, {
    onDelete: 'CASCADE',
    createForeignKeyConstraints: true,
  })
  @JoinColumn({ name: 'id', referencedColumnName: 'parentGroupId' })
  readonly parent?: Group | null;

  @CreateDateColumn(createdAt)
  readonly createdAt?: Date;

  @ManyToMany(() => Privilege)
  readonly privileges?: Privilege[];
}
