import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';

import { Client as IClient } from '../types';
import { schema } from '../common';
import { User } from './user';

@Entity({ ...schema, name: 'clients' })
export class Client implements Readonly<Partial<IClient>> {
  @PrimaryColumn({ type: 'uuid', name: 'user_id' })
  readonly userId?: string;

  @Column({ type: 'uuid', name: 'group_id' })
  readonly groupId?: string;

  @Column('jsonb')
  readonly info?: unknown;

  @OneToOne(() => User, (user: User) => user.client, {
    onDelete: 'RESTRICT',
    createForeignKeyConstraints: true,
  })
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  readonly user?: User;
}
