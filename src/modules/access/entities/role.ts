import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Role as IRole } from '../types';
import { schema } from '../common';
import { createdAt, label, nullable } from '../../../common/entity';

@Entity({ ...schema, name: '' })
export class Role implements Readonly<Partial<IRole>> {
  @PrimaryGeneratedColumn('uuid')
  readonly id?: string;

  @Column({ ...nullable, type: 'uuid', name: 'parent_role_id' })
  readonly parentRoleId?: string | null;

  @Column(label)
  readonly name?: string;

  @Column({ ...nullable, type: 'text' })
  readonly description?: string | null;

  @CreateDateColumn(createdAt)
  readonly createdAt?: Date;
}
