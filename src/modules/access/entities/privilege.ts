import { Column, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm';

import { Privilege as IPrivilege } from '../types';
import { schema } from '../common';
import { label, nullable } from 'common/entity';

@Unique('privilege_name_idx', ['name'])
@Entity({ ...schema, name: 'privileges' })
export class Privilege implements Readonly<Partial<IPrivilege>> {
  @PrimaryGeneratedColumn('uuid')
  readonly id?: string;

  @Column(label)
  readonly name?: string;

  @Column('text')
  readonly route?: string | null;

  @Column({ ...nullable, type: 'text' })
  readonly description?: string | null;

  @Column({ ...label, array: true })
  readonly views?: string[];
}
