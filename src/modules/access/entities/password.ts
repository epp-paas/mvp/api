import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { createHash } from 'crypto';
import FValidator, {
  ValidationSchema,
  RuleString,
  ValidationError,
} from 'fastest-validator';
import { BadRequestException } from '@nestjs/common';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const Validator = require('fastest-validator');

import { Password as IPassword } from '../types';
import { User } from './user';
import { createdAt, nullable } from '../../../common/entity';
import { schema } from '../common';
import { passwordRegs } from '../../../common/util';

const passwordValidationSchema: ValidationSchema<{ password: string }> = {
  password: {
    type: 'multi',
    rules: passwordRegs.map<RuleString>(({ regex, msg }) => ({
      type: 'string',
      pattern: new RegExp(regex, 'mg'),
      messages: { stringPattern: msg },
    })),
  },
};

const validator: FValidator = new Validator();

const passwordValidator = validator.compile(passwordValidationSchema);

@Unique('passwords_idx', ['userId', 'passwordHash'])
@Entity({ ...schema, name: 'passwords' })
export class Password
  implements Readonly<Partial<Omit<IPassword, 'password'>>> {
  @PrimaryGeneratedColumn('uuid')
  readonly id?: string;

  @Column({ type: 'uuid', name: 'user_id' })
  readonly userId?: string;

  @CreateDateColumn(createdAt)
  readonly createdAt?: Date;

  @Column({
    type: 'varchar',
    length: 128,
    name: 'password_hash',
    transformer: {
      from(): string {
        return '';
      },
      to(value: string): string {
        const validated: ValidationError[] | true = passwordValidator(value);

        if (validated !== true) {
          throw new BadRequestException(validated);
        }

        return createHash('sha512').update(value).digest().toString();
      },
    },
  })
  readonly password?: string;

  @Column({ ...nullable, name: 'expired_at', type: 'timestamptz' })
  readonly expiredAt?: Date | null;

  @ManyToOne(() => User, undefined, {
    onDelete: 'CASCADE',
    createForeignKeyConstraints: true,
  })
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  readonly user?: User;
}
