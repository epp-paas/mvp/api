import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryColumn,
} from 'typeorm';

import { Partner as IPartner } from '../types';
import { schema } from '../common';
import { createdAt } from 'common/entity';
import { User } from './user';

@Entity({ ...schema, name: 'partners' })
export class Partner implements Readonly<Partial<IPartner>> {
  @PrimaryColumn('uuid')
  readonly userId?: string;

  @Column('jsonb')
  readonly info?: unknown;

  @CreateDateColumn(createdAt)
  readonly createdAt?: Date;

  @OneToOne(() => User, undefined, {
    onDelete: 'RESTRICT',
    createForeignKeyConstraints: true,
  })
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  readonly user?: User;
}
