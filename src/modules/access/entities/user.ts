import {
  Column,
  CreateDateColumn,
  Entity,
  OneToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { createdAt, label } from 'common/entity';

import { AuthMode, User as IUser } from '../types';
import { Client } from './client';

@Unique('login_idx', ['login', 'mode'])
@Entity({ name: 'users', schema: 'access' })
export class User implements Readonly<Partial<IUser>> {
  @PrimaryGeneratedColumn('uuid')
  readonly id?: string;

  @Column(label)
  readonly login?: string;

  @Column({ type: 'enum', enum: AuthMode })
  readonly mode?: AuthMode;

  @CreateDateColumn(createdAt)
  readonly createdAt?: Date;

  @OneToOne(() => Client, (client: Client) => client.user, {
    onDelete: 'NO ACTION',
  })
  readonly client?: Client | null;
}
