import { EntityOptions } from 'typeorm';

export const schema: EntityOptions = Object.freeze({ schema: 'access' });
