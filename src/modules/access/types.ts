export interface User {
  id: string;
  mode: AuthMode;
  login: string;
  createdAt: Date;
}

export interface Partner {
  userId: string;
  info: unknown;
  createdAt: Date;
}

export interface Client {
  userId: string;
  groupId: string;
  info: unknown;
}

export type ContactType = 'email' | 'work_phone' | 'whatsapp' | 'telegram';

export interface Contact {
  type: ContactType;
  value: string;
}

export interface PersonalData {
  firstNameNom: string;
  firstNameGen?: string;
  firstNameDat?: string;
  firstNameAcc?: string;
  firstNameCra?: string;
  firstNamePre?: string;
  secondNameNom?: string;
  secondNameGen?: string;
  secondNameDat?: string;
  secondNameAcc?: string;
  secondNameCra?: string;
  secondNamePre?: string;
  lastNameNom: string;
  lastNameGen?: string;
  lastNameDat?: string;
  lastNameAcc?: string;
  lastNameCra?: string;
  lastNamePre?: string;
  nicknameNom?: string;
  nicknameGen?: string;
  nicknameDat?: string;
  nicknameAcc?: string;
  nicknameCra?: string;
  nicknamePre?: string;
}

export interface Owner {
  personalData: PersonalData;
  contacts: Contact[];
}

export interface Group {
  id?: string;
  ownerId: string;
  name: string;
  parentGroupId: string | null;
  description: string | null;
  createdAt: Date;
}

export interface Role {
  id: string;
  parentRoleId?: string | null;
  name: string;
  description: string | null;
  createdAt: Date;
}

export enum PrivilegeRules {
  ALLOW,
  DENY,
}

export interface Privilege {
  id: string;
  name: string;
  description: string | null;
  route: string | null;
  views: string[];
}

export interface RolesPrivileges {
  roleId: string;
  privilegeId: string;
  rule: PrivilegeRules;
}

export interface UserGroupRoles {
  userId: string;
  groupId: string;
  roleId: string;
}

export enum AuthMode {
  CLIENT,
  PARTNER,
  OWNER = 777,
}

export interface Session {
  token: string;
  refreshToken: string;
  lastRefresh: Date;
  userId: string;
  passwordId: string;
  mode: AuthMode;
  privileges: {
    [groupId: string]: {
      actions: { allow: string[]; deny: string[] };
      views: { allow: string[]; deny: string[] };
    };
  };
  createdAt: Date;
  expiredAt: Date | null;
}

export interface Password {
  id: string;
  userId: string;
  password: string;
  createdAt: Date;
  expiredAt: Date | null;
}

export enum LogAction {
  REQUEST,
  RESPONSE,
}

// TODO: research what useful info about technical client can be gotten (ip, client type, etc.)
export interface UserLog<C = unknown> {
  requestId: string;
  createdAt: Date;
  sessionToken: string;
  action: LogAction;
  method: string;
  content: C | null;
  clientInfo: unknown | null;
}
