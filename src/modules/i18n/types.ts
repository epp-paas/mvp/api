export interface Language {
  name: string;
  locale: string;
  active: boolean;
  default: boolean;
}

export interface Locale {
  key: string;
  locale: string;
  value: string;
  createdAt: Date;
}
