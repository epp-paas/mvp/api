import { Column, CreateDateColumn, PrimaryColumn } from 'typeorm';

import { Locale as ILocale } from '../types';

export class Locale implements Readonly<ILocale> {
  @PrimaryColumn()
  readonly key: string;

  @PrimaryColumn()
  readonly locale: string;

  @Column()
  value: string;

  @CreateDateColumn({ type: 'timestamptz' })
  createdAt: Date;
}
