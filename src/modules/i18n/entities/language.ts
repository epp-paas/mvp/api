import { Column, PrimaryColumn } from 'typeorm';

import { Language as ILanguage } from '../types';

export class Language implements Readonly<ILanguage> {
  @PrimaryColumn()
  readonly locale: string;

  @Column()
  readonly name: string;

  @Column({ default: false })
  readonly active: boolean;

  @Column({ default: false })
  readonly default: boolean;
}
