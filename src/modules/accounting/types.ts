export interface Merchant<C = unknown> {
  id: string;
  name: string;
  description: string | null;
  active: boolean;
  config: C;
  createdAt: Date;
  expiredAt: Date | null;
}

export interface Account {
  userId: string;
  balance: number;
  currency: string | null;
  lastOperation: string;
}

export interface Binding<C = unknown> {
  userId: string;
  name: string;
  cardInfo: C;
  merchantId: string;
  default: boolean;
  createdAt: Date;
  expiredAt: Date | null;
}

export interface AccountOperation {
  userId: string;
  createdAt: Date;
  diff: number;
  failReason: string | null;
  description: string;
  info: unknown;
}

export enum InvoiceStatus {
  CANCELED,
  CREATED,
  PAID,
}

export interface Invoice {
  id: string;
  status: InvoiceStatus;
  log: { [key: string]: string };
  userId: string;
  amount: number;
  currency: string | null;
  createdAt: Date;
}

export enum PaymentStatus {
  CLOSED,
  CREATED,
  PROCESSING,
  HOLD,
  ACCEPTED,
  SUCCESS,
  ERROR,
}

export interface Payment {
  id: string;
  invoiceId: string;
  userId: string;
  merchantId: string | null;
  bindId: string | null;
  paymentMethod: string;
  status: PaymentStatus;
  amount: number;
  currency: string;
  createdAt: Date;
}

export interface PaymentHistory {
  paymentId: string;
  createdAt: Date;
  archivedAt: Date;
  userId: string;
  invoiceId: string;
  status: PaymentStatus;
  amount: number;
  currency: string;
  log: PaymentLog[];
}

export interface PaymentLog<D = unknown> {
  paymentId: string;
  data: D;
  createdAt: Date;
}
