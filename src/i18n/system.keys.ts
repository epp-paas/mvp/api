export type Security =
  | 'security:password_required_capital'
  | 'security:password_required_lowercase'
  | 'security:password_required_number'
  | 'security:password_required_symbols'
  | 'security:password_minimal_length';

export const PSD_REQ_CAP: Security = 'security:password_required_capital';
export const PSD_REQ_LOW: Security = 'security:password_required_lowercase';
export const PSD_REQ_NUM: Security = 'security:password_required_number';
export const PSD_REQ_SYM: Security = 'security:password_required_symbols';
export const PSD_MIN_LEN: Security = 'security:password_minimal_length';

export type Message = Security;
